//
//  CustomCell.swift
//  TicTacToe
//
//  Created by Dileep on 8/6/17.
//
//

import UIKit

class CustomCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
}
