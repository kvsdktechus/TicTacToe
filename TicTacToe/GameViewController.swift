//
//  GameViewController.swift
//  TicTacToe
//
//  Created by Dileep on 8/5/17.
//
//

import UIKit

class GameViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    
    let reuseIdentifier = "Cell"
    let itemsPerRow: CGFloat = 3
    let paddingSpace: CGFloat = 3
    
    var game = TicTacToeGame()
    var restartButton = UIBarButtonItem()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = game.gameState.rawValue
        restartButton = UIBarButtonItem.init(title: "Restart", style: .plain, target: self, action: #selector(restartGame))
    }

    //Resets the game to initial state
    @objc func restartGame() {
        game = TicTacToeGame()
        collectionView.reloadData()
        self.navigationItem.rightBarButtonItem = nil
    }
    
    // Invalidate the flow layout on size transition inorder to layout collectionview properly
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        
        guard let flowLayout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout else { return }
        flowLayout.invalidateLayout()
    }

    // Update the View based on the model state
    func updateView(for cell: CustomCell, indexPath: IndexPath) {
        switch game.gameBoard[indexPath.row] {
        case .None:
            cell.imageView.image = nil
        case .X:
            cell.imageView.image = #imageLiteral(resourceName: "cross")
        case .O:
            cell.imageView.image = #imageLiteral(resourceName: "nought")
        }
        
        //Update the title of the screen with the game state
        self.title = game.gameState.rawValue
        
        // Add the Restart button to the right navigation bar button when the game is over
        if game.gameState == .XWon || game.gameState == .OWon || game.gameState == .Tie {
            self.navigationItem.rightBarButtonItem = restartButton
        }
    }
}


extension GameViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return game.gameBoard.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! CustomCell
        cell.backgroundColor = UIColor.white
        updateView(for: cell, indexPath: indexPath)
        return cell
    }
}


extension GameViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let cell = collectionView.cellForItem(at: indexPath) as! CustomCell
        // Update the game object when player pressed on a square
        game.pressedSquare(index: indexPath.row)
        updateView(for: cell, indexPath: indexPath)
    }
    
}

extension GameViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let itemsize = (collectionView.bounds.width / itemsPerRow) - paddingSpace
        return CGSize(width: itemsize, height: itemsize)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5.0
    }
}

