//
//  TicTacToeGame.swift
//  TicTacToe
//
//  Created by Dileep on 8/6/17.
//
//

import UIKit

enum GameState: String {
    case XTurn = "X's Turn"
    case OTurn = "O's Turn"
    case XWon  = "X Wins"
    case OWon  = "O Wins"
    case Tie   = "Tie Game"
}

class TicTacToeGame: NSObject {
    
    enum MarkType: String {
        case None = "_"
        case X = "X"
        case O = "O"
    }
    
    var gameBoard: [MarkType]
    var gameState: GameState
    
    //Initializer Methods
        
    override init() {
        gameBoard = [MarkType](repeating: .None, count: 9)
        gameState = .XTurn
    }
    
    init(_ board: [MarkType] = [MarkType](repeating: .None, count: 9), firstPlayer: GameState = .XTurn) {
        gameBoard = board
        gameState = firstPlayer
    }
    
    // When player pressed on any of the square
    func pressedSquare(index: Int) {
        if gameBoard[index] != .None {
            return
        }
        if gameState == .XTurn {
            gameBoard[index] = .X
            gameState = .OTurn
            checkForGameOver()
        } else if gameState == .OTurn {
            gameBoard[index] = .O
            gameState = .XTurn
            checkForGameOver()
        }
    }
    
    // Check and update the final gamestate if the game is over
    func checkForGameOver() {
        if !gameBoard.contains(.None) {
            gameState = .Tie
            return
        }
        
        var winningCombinations = [String]()
        // Horizontal
        winningCombinations.append(getGameString(indices: [0, 1, 2]))
        winningCombinations.append(getGameString(indices: [3, 4, 5]))
        winningCombinations.append(getGameString(indices: [6, 7, 8]))
        // Vertical
        winningCombinations.append(getGameString(indices: [0, 3, 6]))
        winningCombinations.append(getGameString(indices: [1, 4, 7]))
        winningCombinations.append(getGameString(indices: [2, 5, 8]))
        // Diagonal
        winningCombinations.append(getGameString(indices: [0, 4, 8]))
        winningCombinations.append(getGameString(indices: [2, 4, 6]))
        
        for combination in winningCombinations {
            if combination == "XXX" {
                gameState = .XWon
                return
            } else if combination == "OOO" {
                gameState = .OWon
                return
            }
        }
    }
    
    // Returns the gamestring based on the square indices passed
    func getGameString(indices: [Int] = [0, 1, 2, 3, 4, 5, 6, 7, 8]) -> String {
        var gameString = ""
        for index in indices {
            gameString += gameBoard[index].rawValue
        }
        return gameString
    }
    
}
