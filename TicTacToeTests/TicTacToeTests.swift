//
//  TicTacToeTests.swift
//  TicTacToeTests
//
//  Created by Dileep on 8/5/17.
//
//

import XCTest
@testable import TicTacToe

class TicTacToeTests: XCTestCase {
    
    func testFirstPlayerIsOThenGameStateIsXTurn() {
        let game = TicTacToeGame(firstPlayer: .OTurn)
        game.pressedSquare(index: 0)
        XCTAssertEqual(game.gameBoard[0], .O)
        XCTAssertEqual(game.gameState, .XTurn)
    }
    
    func testFirstPlayerIsXThenGameStateIsOTurn() {
        let game = TicTacToeGame()
        game.pressedSquare(index: 0)
        XCTAssertEqual(game.gameBoard[0], .X)
        XCTAssertEqual(game.gameState, .OTurn)
    }
    
    func testTopRowForXWonAndGameStringIsXXX() {
        let game = TicTacToeGame([.X, .X, .None,
                                  .O, .O, .None,
                                  .None, .None, .None ])
        game.pressedSquare(index: 2)
        XCTAssertEqual(game.gameState, .XWon)
        XCTAssertEqual(game.getGameString(indices: [0, 1, 2]), "XXX")
    }
    
    func testMiddleColumnForOWonAndGameStringIsOOO() {
        let game = TicTacToeGame([.X, .O, .X,
                                  .X, .O, .None,
                                  .None, .None, .None], firstPlayer: .OTurn)
        game.pressedSquare(index: 7)
        XCTAssertEqual(game.gameState, .OWon)
        XCTAssertEqual(game.getGameString(indices: [1, 4, 7]), "OOO")
    }
    
    func testTopLeftToBottomRightDiagonalForXWonAndGameStringIsXXX() {
        let game = TicTacToeGame([.X, .O, .None,
                                  .None, .X, .O,
                                  .None, .None, .None])
        game.pressedSquare(index: 8)
        XCTAssertEqual(game.gameState, .XWon)
        XCTAssertEqual(game.getGameString(indices: [0, 4, 8]), "XXX")
    }
    
    func testTieGame() {
        let game = TicTacToeGame([.X, .O, .X,
                                  .O, .X, .None,
                                  .O, .X, .O])
        game.pressedSquare(index: 5)
        XCTAssertEqual(game.gameState, .Tie)
        XCTAssertEqual(game.getGameString(), "XOXOXXOXO")
    }
    
}
