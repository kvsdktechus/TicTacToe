# TicTacToe for iOS using collection view

This is a Tic Tac Toe game created in Swift, the iOS programming language.

**Game Features:** 

* Allows two players to compete against each other.
* Game would continue until either one player wins or a tie is reached
* Status of the game is updated in the navigation title of the screen
* Once a game is concluded a Restart button appears in place of right navigation bar button
* Player can click on Restart button to start a new game and the button disappears
* State of the app is not saved and everytime you launch the app it would start a new game


**Implementation:**

For the Tic Tac Toe game the following technologies were used:
* Xcode with storyBoard to visually design the app elements
* Swift programming language with Apples UIKit framework for app development

**Build and Runtime Requirements:**

* Mac running MacOS 10.12 or later
* iOS 9 or later
* Xcode 8.3.3 and Swift 3
* Tested on iPhone 4s and later, iPad Air and later
* Works in both Portrait and landscape orientation for all the above devices